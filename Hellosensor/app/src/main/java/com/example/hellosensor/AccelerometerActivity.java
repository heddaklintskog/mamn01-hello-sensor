package com.example.hellosensor;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

import java.text.DecimalFormat;

public class AccelerometerActivity extends AppCompatActivity implements SensorEventListener {


    private TextView textX, textY, textZ, tilt, tilt2;
    private Sensor mySensor;
    private SensorManager SM;
    private final MediaPlayer mp = new MediaPlayer();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);
        //create a sensorManager
        SM = (SensorManager)getSystemService(SENSOR_SERVICE);
        //Accelerometer sensor
        mySensor = SM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        SM.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL);
        textX = (TextView)findViewById(R.id.textX);
        textY = (TextView)findViewById(R.id.textY);
        textZ = (TextView)findViewById(R.id.textZ);
        tilt = (TextView)findViewById(R.id.tilt);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        DecimalFormat df = new DecimalFormat("##.##");

        ObjectAnimator animationRight = ObjectAnimator.ofFloat(tilt, "translationX", 200f);
        animationRight.setDuration(200);
        ObjectAnimator animationLeft = ObjectAnimator.ofFloat(tilt, "translationX", -200f);
        animationLeft.setDuration(200);
        ObjectAnimator animationMiddle = ObjectAnimator.ofFloat(tilt, "translationX", 0f);
        animationMiddle.setDuration(0);
        ObjectAnimator animationDown = ObjectAnimator.ofFloat(tilt, "translationY", 100f);
        animationDown.setDuration(100);
        ObjectAnimator animationUp = ObjectAnimator.ofFloat(tilt, "translationY", 0f);
        animationUp.setDuration(100);



        textX.setText("X = "+df.format(event.values[0]));
        textY.setText("Y = "+df.format(event.values[1]));
        textZ.setText("Z = "+df.format(event.values[2]));
        double d = event.values[0];


        if( d<0.15 && d>-0.15){
            tilt.setText("");
            animationMiddle.start();
        }
        else if(d <= -0.15){

            if(!tilt.getText().equals("Right")){
                tilt.setText("Right");
                animationRight.start();
            }

        }else{

            if(!tilt.getText().equals("Left")){
                tilt.setText("Left");
                animationLeft.start();
            }
        }
        if((double) event.values[2]<0){
            animationDown.start();
        }else{
            animationUp.start();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    protected void onResume() {
        super.onResume();
        SM.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        SM.unregisterListener(this);
    }
}
