package com.example.hellosensor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void getCompass(View view) { //developer.android
        Intent intent = new Intent(this, CompassActivity.class);

        startActivity(intent);
    }

    public void getAccelerometer(View view) { //developer.android
        Intent intent = new Intent(this, AccelerometerActivity.class);

        startActivity(intent);
    }
}
